package com.mgarnier11.tpandroid.interfaces;

import com.mgarnier11.tpandroid.classes.Card;
import com.mgarnier11.tpandroid.fragments.CardFragment;

public interface ICardClickInterface {
    public void onCardClicked(CardFragment frg,  Card card);
}
