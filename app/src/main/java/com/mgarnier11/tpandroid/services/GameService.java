package com.mgarnier11.tpandroid.services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import com.mgarnier11.tpandroid.activities.GameActivity;

import java.util.Timer;
import java.util.TimerTask;

public class GameService extends Service {
    public GameService() {
    }

    public class MyBinder extends Binder {
        GameService getService() {
            return GameService.this;
        }
    }
    private final MyBinder myBinder = new MyBinder();
    @Override
    public IBinder onBind(Intent intent) {
        return myBinder;
    }

    private int actualTime = 0;
    private Handler handler = new Handler();

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        handler.postDelayed(rCallback, 1000);

        return START_STICKY;
    }

    private Runnable rCallback = new Runnable() {
        @Override
        public void run() {
            actualTime++;

            Intent intent = new Intent(GameActivity.BROADCAST);
            intent.putExtra("timeTick", actualTime);
            sendBroadcast(intent);

            Log.d("DEBUG", "service ticking : " + actualTime);

            handler.postDelayed(this, 1000);
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();

        handler.removeCallbacks(rCallback);
    }
}