package com.mgarnier11.tpandroid.classes;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Game {
    private Date startDate = new Date();
    private Date finishDate;
    private boolean win;
    private int maxDuration;
    private int sizeX;
    private int sizeY;
    private ArrayList<Card> cardList = new ArrayList<>();

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }

    public int getMaxDuration() {
        return maxDuration;
    }

    public void setMaxDuration(int maxDuration) {
        this.maxDuration = maxDuration;
    }

    public ArrayList<Card> getCardList() {
        return cardList;
    }

    public int getSizeX() {
        return sizeX;
    }

    public void setSizeX(int sizeX) {
        this.sizeX = sizeX;
    }

    public int getSizeY() {
        return sizeY;
    }

    public void setSizeY(int sizeY) {
        this.sizeY = sizeY;
    }

    public boolean isWin() {
        return win;
    }

    public void setWin(boolean win) {
        this.win = win;
    }

    public void setCardList(ArrayList<Card> cardList) {
        this.cardList = cardList;
    }

    public Game() {
    }

    public boolean isFinished() {
        boolean finished = true;

        for (Card c: cardList) {
            if (!c.isDrawn()) {
                finished = false;
                break;
            }
        }

        return finished;
    }

    public int findCardIndex(Card c) {
        for (int i = 0; i < cardList.size(); i++) {
            Card ca = cardList.get(i);

            if (c.getImageSource().equals(ca.getImageSource()) && c.getPosition().equals(ca.getPosition())) {
                return i;
            }
        }
        return -1;
    }

    public static Game generateNewGame(int maxDuration, int sizeX, int sizeY, int nbDifferentCards) throws Exception {
        Game g = new Game();

        g.setMaxDuration(maxDuration);
        g.setSizeX(sizeX);
        g.setSizeY(sizeY);


        CardImageLoader cardImageLoader = CardImageLoader.getInstance();

        ArrayList<String> availableImages = cardImageLoader.generateRandomImagesList(nbDifferentCards);

        ArrayList<Position> availablePositions = new ArrayList<Position>();

        for (int x = 0; x < sizeX; x++) {
            for (int y = 0; y < sizeY; y++) {
                availablePositions.add(new Position(x, y));
            }
        }

        Random rnd = new Random();

        for (int i = 0; i < sizeX * sizeY; i++) {
            int imageIndex = i % nbDifferentCards;

            String image = availableImages.get(imageIndex);

            int positionIndex = rnd.nextInt(availablePositions.size());

            Position p = availablePositions.get(positionIndex);

            availablePositions.remove(positionIndex);

            g.getCardList().add(new Card(p, false, image));
        }

        return g;

    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().create();

        return gson.toJson(this);
    }

    public static Game fromString(String input) {
        Gson gson = new GsonBuilder().create();

        return gson.fromJson(input, Game.class);
    }
}
