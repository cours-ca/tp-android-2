package com.mgarnier11.tpandroid.classes;

public enum GameMaxDuration {
    _60(60, "duration_60"),
    _120(120, "duration_120"),
    _180(180, "duration_180"),
    _240(240, "duration_240"),
    _300(300, "duration_300");

    public final int duration;
    public final String label;

    GameMaxDuration(int duration, String label) {
        this.duration = duration;
        this.label = label;
    }
}
