package com.mgarnier11.tpandroid.classes;

import android.content.Context;
import android.content.SharedPreferences;

import com.mgarnier11.tpandroid.R;

public class LocalGameHelper {
    private static LocalGameHelper instance;

    public static LocalGameHelper getInstance(Context ctx) {
        if (LocalGameHelper.instance == null) {
            LocalGameHelper.instance = new LocalGameHelper(ctx);
        }
        return LocalGameHelper.instance;
    }

    private LocalGameHelper(Context ctx) {
        context = ctx;
    }

    private Context context;

    public void saveGame(Game game) {
        SharedPreferences sp = context.getSharedPreferences(context.getString(R.string.game_shared_preferences), Context.MODE_PRIVATE);

        SharedPreferences.Editor e = sp.edit();

        if (game != null) {
            e.putString("savedGame", game.toString());
        } else {
            e.putString("savedGame", "");
        }

        e.apply();
    }

    public Game getLastSavedGame() throws Exception {
        SharedPreferences sp = context.getSharedPreferences(context.getString(R.string.game_shared_preferences), Context.MODE_PRIVATE);

        String gameStr = sp.getString("savedGame", "");

        if (!gameStr.equals("")) {
            return Game.fromString(gameStr);
        } else {
            throw new Exception("no game saved");
        }
    }

    public boolean hasSavedGame() {
        SharedPreferences sp = context.getSharedPreferences(context.getString(R.string.game_shared_preferences), Context.MODE_PRIVATE);

        return !sp.getString("savedGame", "").equals("");
    }

    public boolean hasGameHistory() {
        SharedPreferences sp = context.getSharedPreferences(context.getString(R.string.game_shared_preferences), Context.MODE_PRIVATE);

        return !sp.getString("gameHistory", "").equals("");
    }

    public void addGameToHistory(Game game) {

    }

//    public ArrayList<Game> getGameHistory() {
//
//    }
}
