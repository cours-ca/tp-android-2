package com.mgarnier11.tpandroid.classes;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;

import com.mgarnier11.tpandroid.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class CardImageLoader {
    private static CardImageLoader instance;

    public static CardImageLoader initCardImageLoader(Context ctx) {
        if (instance == null) {
            instance = new CardImageLoader(ctx);
        }
        return instance;
    }

    public static CardImageLoader getInstance() throws Exception {
        if (instance != null) {
            return instance;
        } else {
            throw new Exception("CardImageLoader instance has not been defined yet");
        }
    }

    private Context androidContext;
    private ArrayList<String> fullImageList = new ArrayList<String>();

    private CardImageLoader(Context ctx) {
        androidContext = ctx;

        Resources r = androidContext.getResources();

        String[] list = r.getStringArray(R.array.champions);

        for (String image: list) {
            fullImageList.add(image);
        }
    }

    public ArrayList<String> generateRandomImagesList(int nbDifferentImages) {
        Collections.shuffle(fullImageList);

        ArrayList<String> retValues = new ArrayList<>();

        for (int i = 0; i < nbDifferentImages; i++) {
            retValues.add(fullImageList.get(i));
        }

        return retValues;
    }
}
