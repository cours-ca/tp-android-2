package com.mgarnier11.tpandroid.classes;

public enum GameType {
    _3x2(3,2),
    _4x4(4,4),
    _6x6(6, 6),
    _8x8(8, 8),
    _10x10(10, 10);
//    _12x12(12, 12);
//    _16x16(16, 16),
//    _20x20(20, 20);



    public final int sizeX;
    public final int sizeY;

    GameType(int sizeX, int sizeY) {
        this.sizeX = sizeX;
        this.sizeY = sizeY;
    }

}
