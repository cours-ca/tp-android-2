package com.mgarnier11.tpandroid.classes;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Card {
    private Position position;
    private boolean drawn;
    private String imageSource;

    public boolean isDrawn() {
        return drawn;
    }

    public void setDrawn(boolean drawn) {
        this.drawn = drawn;
    }

    public String getImageSource() {
        return imageSource;
    }

    public void setImageSource(String imageSource) {
        this.imageSource = imageSource;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Card(Position position, boolean drawn, String championId) {
        this.position = position;
        this.drawn = drawn;
        this.imageSource = "https://ddragon.leagueoflegends.com/cdn/11.14.1/img/champion/" + championId + ".png";
    }

    public String toString() {
        Gson gson = new GsonBuilder().create();

        return gson.toJson(this);
    }

    public static Card fromString(String input) {
        Gson gson = new GsonBuilder().create();

        return gson.fromJson(input, Card.class);
    }
}
