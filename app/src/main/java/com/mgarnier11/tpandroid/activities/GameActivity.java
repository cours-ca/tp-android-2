package com.mgarnier11.tpandroid.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentContainerView;
import androidx.gridlayout.widget.GridLayout;


import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Debug;
import android.os.Handler;
import android.util.Log;
import android.util.Size;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mgarnier11.tpandroid.R;
import com.mgarnier11.tpandroid.classes.Card;
import com.mgarnier11.tpandroid.classes.Game;
import com.mgarnier11.tpandroid.classes.LocalGameHelper;
import com.mgarnier11.tpandroid.fragments.CardFragment;
import com.mgarnier11.tpandroid.interfaces.ICardClickInterface;
import com.mgarnier11.tpandroid.services.GameService;

import java.util.ArrayList;
import java.util.Date;

public class GameActivity extends AppCompatActivity implements ICardClickInterface {
    public static final String BROADCAST = "com.mgarnier11.gameService";

    private LocalGameHelper localGameHelper;

    private Game game;
    private int actualTime = 0;

    private GridLayout cardListGridLayout;
    private TextView timeRemainingTextView;

    private Card firstCardClicked;
    private Card secondCardClicked;

    private ArrayList<CardFragment> frgList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = this.getIntent();

        if (intent != null) {
            Bundle b = intent.getExtras();

            String gameStr = b.getString("newGame");

            game = Game.fromString(gameStr);
        } else {
            finish();
        }

        localGameHelper = LocalGameHelper.getInstance(getApplicationContext());

        initLayout();
    }

    @Override
    protected void onStart() {
        super.onStart();

        Intent intentService = new Intent(this, GameService.class);

        startService(intentService);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Intent intentService = new Intent(this, GameService.class);

        stopService(intentService);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, new IntentFilter(GameActivity.BROADCAST));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    private void initLayout() {
        setContentView(R.layout.activity_game);

        cardListGridLayout = findViewById(R.id.gameActGridLayoutCardList);
        timeRemainingTextView = findViewById(R.id.gameActTextViewTimeRemaining);

        timeRemainingTextView.setText(game.getMaxDuration() + " s");

        Point screenSize = new Point();

        getWindowManager().getDefaultDisplay().getSize(screenSize);

        int fullWidth = screenSize.x;
        int fullHeight = fullWidth;
        int cardWidth = (int) (fullWidth / game.getSizeY());
        int cardHeight = (int) (fullHeight / game.getSizeX());

        Log.d("DEBUG", "Full width : " + fullWidth);

        cardListGridLayout.setRowCount(game.getSizeX());
        cardListGridLayout.setColumnCount(game.getSizeY());

        frgList = new ArrayList<>();

        for (int i = 0; i < game.getCardList().size(); i++) {
            Card c = game.getCardList().get(i);

            int frgId = (i + 1) * 9999;

            FragmentContainerView frgView = new FragmentContainerView(this);

            frgView.setId(frgId);

            cardListGridLayout.addView(frgView, new GridLayout.LayoutParams(
                    GridLayout.spec(c.getPosition().x),
                    GridLayout.spec(c.getPosition().y)
            ));

            CardFragment frg = CardFragment.createInstance(cardWidth, cardHeight);

            frg.setInterface(this);
            frg.setCard(c);

            frgList.add(frg);

            getSupportFragmentManager().beginTransaction().add(frgView.getId(), frg).commit();

            Log.d("DEBUG", "view id : " + frg.getId());

        }

        Log.d("DEBUG", "child count : " + cardListGridLayout.getChildCount());
    }

    private void updateAllFrgImages() {
        for (CardFragment frg : frgList) {
            frg.updateImage();
        }
    }

    private boolean areImagesEqual() {
        if (firstCardClicked != null && secondCardClicked != null) {
            return firstCardClicked.getImageSource().equals(secondCardClicked.getImageSource());
        } else {
            return false;
        }
    }

    private void checkGameFinished() {
        if (game.isFinished() || actualTime >= game.getMaxDuration()) {
            Intent i = new Intent(this, GameResultActivity.class);

            if (game.isFinished()) {
                i.putExtra("win", true);
                game.setWin(true);
            } else {
                i.putExtra("win", false);
                game.setWin(false);
            }

            startActivity(i);

            game.setFinishDate(new Date());

            localGameHelper.saveGame(null);
            localGameHelper.addGameToHistory(game);

            finish();
        }
    }

    @Override
    public void onCardClicked(CardFragment frg, Card card) {
        if (!card.isDrawn()) {
            if (firstCardClicked == null) {
                firstCardClicked = card;
                card.setDrawn(true);
            } else if (secondCardClicked == null) {
                secondCardClicked = card;
                card.setDrawn(true);

                new Handler().postDelayed(() -> {
                    Log.d("DEBUG", firstCardClicked + " " + secondCardClicked);
                    if (firstCardClicked != null && secondCardClicked != null && !areImagesEqual()) {
                        firstCardClicked.setDrawn(false);
                        secondCardClicked.setDrawn(false);
                    } else {
                        localGameHelper.saveGame(game);
                    }

                    firstCardClicked = null;
                    secondCardClicked = null;

                    updateAllFrgImages();
                }, areImagesEqual() ? 5 : 500);
            }

            frg.updateImage();
        }

        Log.d("DEBUG", frg.getId() + " : " + card.getImageSource());
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                actualTime = bundle.getInt("timeTick");

                timeRemainingTextView.setText(game.getMaxDuration() - actualTime + " s");

                checkGameFinished();

            }
        }
    };
}