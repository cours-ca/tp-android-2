package com.mgarnier11.tpandroid.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.mgarnier11.tpandroid.R;
import com.mgarnier11.tpandroid.classes.CardImageLoader;
import com.mgarnier11.tpandroid.classes.Game;
import com.mgarnier11.tpandroid.classes.LocalGameHelper;

public class MainActivity extends AppCompatActivity {

    private LocalGameHelper localGameHelper;

    private boolean hasSavedGame = false;
    private boolean hasGameHistory = false;

    private Button newGameButton;
    private Button loadGameButton;
    private Button gameHistoryButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CardImageLoader.initCardImageLoader(getApplicationContext());
        localGameHelper = LocalGameHelper.getInstance(getApplicationContext());

        this.initLayout();
    }

    @Override
    protected void onResume() {
        super.onResume();

        hasSavedGame = localGameHelper.hasSavedGame();

        hasGameHistory = localGameHelper.hasGameHistory();

        loadGameButton.setEnabled(hasSavedGame);

        gameHistoryButton.setEnabled(hasGameHistory);
    }

    private void initLayout() {
        setContentView(R.layout.activity_main);

        newGameButton = findViewById(R.id.mainActButtonNewGame);
        loadGameButton = findViewById(R.id.mainActButtonLoadGame);
        gameHistoryButton = findViewById(R.id.mainActButtonGameHistory);

        newGameButton.setOnClickListener(v -> {
           startCreateGameActivity();
        });

        gameHistoryButton.setOnClickListener(v -> {
            startGameHistoryActivity();
        });

        loadGameButton.setOnClickListener(v-> {
            startGameActivity();
        });
    }

    private void startCreateGameActivity() {
        Intent i = new Intent(this, CreateGameActivity.class);

        startActivity(i);
    }

    private void startGameHistoryActivity() {
        Intent i = new Intent(this, GameHistoryActivity.class);

        startActivity(i);
    }

    private void startGameActivity() {
        Intent i = new Intent(this, GameActivity.class);

        try {
            Game savedGame = localGameHelper.getLastSavedGame();

            Bundle b = new Bundle();

            b.putString("newGame", savedGame.toString());

            i.putExtras(b);
        } catch (Exception e) {
            i = new Intent(this, CreateGameActivity.class);
        }

        startActivity(i);

    }
}