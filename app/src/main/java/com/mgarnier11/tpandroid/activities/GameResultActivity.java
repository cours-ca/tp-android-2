package com.mgarnier11.tpandroid.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.mgarnier11.tpandroid.R;
import com.mgarnier11.tpandroid.classes.Game;
import com.mgarnier11.tpandroid.classes.LocalGameHelper;
import com.squareup.picasso.Picasso;

public class GameResultActivity extends AppCompatActivity {

    private boolean gameResult;

    private ImageView resultImageView;
    private Button gameHistoryButton;
    private Button newGameButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = this.getIntent();

        if (intent != null) {
            Bundle b = intent.getExtras();

            gameResult = b.getBoolean("win");
        } else {
            gameResult = false;
        }

        initLayout();

    }

    private void initLayout() {
        setContentView(R.layout.activity_game_result);

        resultImageView = findViewById(R.id.gameResultActImageViewResult);

        Picasso
                .get()
                .load(gameResult ? R.drawable.victory : R.drawable.defeat)
                .into(resultImageView);

        gameHistoryButton = findViewById(R.id.gameResultActButtonHistory);
        newGameButton = findViewById(R.id.gameResultActButtonNewGame);

        gameHistoryButton.setOnClickListener(this::onGameHistoryButtonClick);
        newGameButton.setOnClickListener(this::onNewGameButtonClick);
    }

    private void onGameHistoryButtonClick(View v) {
        Intent i = new Intent(this, GameHistoryActivity.class);

        startActivity(i);

        finish();
    }

    private void onNewGameButtonClick(View v) {
        Intent i = new Intent(this, CreateGameActivity.class);

        startActivity(i);

        finish();
    }
}