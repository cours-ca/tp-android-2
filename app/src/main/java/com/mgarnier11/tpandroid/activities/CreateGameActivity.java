package com.mgarnier11.tpandroid.activities;

import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.mgarnier11.tpandroid.R;
import com.mgarnier11.tpandroid.classes.Game;
import com.mgarnier11.tpandroid.classes.GameMaxDuration;
import com.mgarnier11.tpandroid.classes.GameType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CreateGameActivity extends AppCompatActivity {

    private Spinner gameSizeSpinner;
    private Spinner maxDurationSpinner;
    private Spinner nbDifferentCardsSpinner;

    private Button startGameButton;
    private Button cancelButton;

    private List<GameType> gameTypeList = Arrays.asList(GameType.values());
    private ArrayList<Integer> availableNumbers;
    private List<GameMaxDuration> gameMaxDurationList = Arrays.asList(GameMaxDuration.values());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.initLayout();
    }

    private void initLayout() {
        setContentView(R.layout.activity_create_game);

        gameSizeSpinner = findViewById(R.id.createGameActSpinnerGameSize);
        maxDurationSpinner = findViewById(R.id.createGameActSpinnerMaxDuration);
        nbDifferentCardsSpinner = findViewById(R.id.createGameActSpinnerNbDifferentCards);

        startGameButton = findViewById(R.id.createGameActButtonStartGame);
        cancelButton = findViewById(R.id.createGameActButtonCancel);

        startGameButton.setOnClickListener(this::onStartGameButtonClick);
        cancelButton.setOnClickListener(this::onCancelButtonClick);

        ArrayList<String> typesLabels = new ArrayList<>();
        for(int i = 0; i < gameTypeList.size(); i++) {
            GameType type = gameTypeList.get(i);
            typesLabels.add(type.sizeX + " x " + type.sizeY);
        }

        ArrayAdapter<String> gameSizeAdapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, typesLabels);

        gameSizeSpinner.setAdapter(gameSizeAdapter);

        updateAvailableNumberOfCards();

        gameSizeSpinner.setOnItemSelectedListener(onGameSizeSpinnerItemSelected);

        ArrayList<String> maxDurationsLabels = new ArrayList<>();
        for(int i = 0; i < gameMaxDurationList.size(); i++) {
            GameMaxDuration duration = gameMaxDurationList.get(i);

            @StringRes int resId = getResources().getIdentifier(duration.label, "string", getPackageName());

            maxDurationsLabels.add(getString(resId));
        }

        ArrayAdapter<String> maxDurationAdapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, maxDurationsLabels);

        maxDurationSpinner.setAdapter(maxDurationAdapter);
    }

    private void updateAvailableNumberOfCards() {
        GameType type =  gameTypeList.get((int)gameSizeSpinner.getSelectedItemId());

        if (type != null) {
            int totalNumberOfCards = type.sizeX * type.sizeY;

            availableNumbers = new ArrayList<Integer>();

            for (int i = totalNumberOfCards / 2; i > 0; i--) {
                if (totalNumberOfCards % i == 0) availableNumbers.add(i);
            }

            nbDifferentCardsSpinner.setSelection(0);

            ArrayAdapter<Integer> adapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, availableNumbers);

            nbDifferentCardsSpinner.setAdapter(adapter);
        }
    }

    private AdapterView.OnItemSelectedListener onGameSizeSpinnerItemSelected = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            updateAvailableNumberOfCards();
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) { }
    };

    private void onStartGameButtonClick(View v) {
        try {
            GameMaxDuration maxDuration = gameMaxDurationList.get(maxDurationSpinner.getSelectedItemPosition());
            GameType gameType = gameTypeList.get(gameSizeSpinner.getSelectedItemPosition());
            int nbDifferentCards = availableNumbers.get(nbDifferentCardsSpinner.getSelectedItemPosition());

            Game newGame = Game.generateNewGame(maxDuration.duration, gameType.sizeX, gameType.sizeY, nbDifferentCards);

            Intent i = new Intent(this, GameActivity.class);
            Bundle b = new Bundle();

            b.putString("newGame", newGame.toString());

            i.putExtras(b);

            startActivity(i);

            finish();
        } catch(Exception e) {
            e.printStackTrace();
        }


    }

    private void onCancelButtonClick(View v) {
        finish();
    }

}