package com.mgarnier11.tpandroid.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.mgarnier11.tpandroid.R;
import com.mgarnier11.tpandroid.classes.Card;
import com.mgarnier11.tpandroid.classes.Game;
import com.mgarnier11.tpandroid.interfaces.ICardClickInterface;
import com.squareup.picasso.Picasso;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CardFragment#createInstance} factory method to
 * create an instance of this fragment.
 */
public class CardFragment extends Fragment {
    private Card card;

    private int cardSizeX;
    private int cardSizeY;

    private FrameLayout frgLayout;
    private ImageView championImageView;

    private ICardClickInterface cardClickInterface;

    public CardFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment CardFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CardFragment createInstance(int sizeX, int sizeY) {
        CardFragment fragment = new CardFragment();
        Bundle args = new Bundle();
        args.putInt("sizeX", sizeX);
        args.putInt("sizeY", sizeY);
        fragment.setArguments(args);
        return fragment;
    }

    public void setInterface(ICardClickInterface i) {
        cardClickInterface = i;
    }

    public void setCard(Card c) {
        card = c;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            cardSizeX = getArguments().getInt("sizeX");
            cardSizeY = getArguments().getInt("sizeY");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_card, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        frgLayout = view.findViewById(R.id.frgCardFrameLayout);

        frgLayout.getLayoutParams().width = cardSizeX;
        frgLayout.getLayoutParams().height = cardSizeY;

        Log.d("DEBUG", "size : " + cardSizeX + ":" + cardSizeY + " | " + card.getImageSource());

        championImageView = view.findViewById(R.id.frgCardImageView);

        championImageView.setOnClickListener(this::onImageClick);

        Picasso
                .get()
                .load(card.getImageSource())
                .resize(cardSizeX, cardSizeY)
                .fetch();

        updateImage();
    }

    public void updateImage() {

        if (card.isDrawn()) {
            Picasso
                    .get()
                    .load(card.getImageSource())
                    .resize(cardSizeX, cardSizeY)
                    .into(championImageView);
        } else {
            Picasso
                    .get()
                    .load(R.drawable.logo_riot)
                    .resize(cardSizeX, cardSizeY)
                    .into(championImageView);
        }
    }

    private void onImageClick(View v) {
        this.cardClickInterface.onCardClicked(this, card);
    }
}